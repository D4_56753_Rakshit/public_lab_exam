const { response } = require('express');
const express=require('express')

const router=express.Router();
const utils=require("../utils")
const db=require("../db")

router.get('/',(request,response)=>{
    const connection=db.openConnection()

    const statement=`select * from movie`

    connection.query(statement,(error,result)=>{
        connection.end();
        if(result.length>0){
            console.log(result.length)
            response.send(utils.createResult(error,result))
        }else{
            response.send("user not found")
        }
    })
})

router.post('/add',(request,response)=>{
    const{movie_id,movie_title,movie_release_date,director_name}=request.body
    const connection=db.openConnection()

    const statement=`insert into movie(movie_id,movie_title,movie_release_date,director_name)
    values(${movie_id},'${movie_title}','${movie_release_date}','${director_name}')

    `

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResult(error,result))
    })
})

router.post('/update/:movie_id',(request,response)=>{
    const{movie_id}=request.params
    const{movie_release_date}=request.body
    

    const statement=`
    update movie set movie_release_date='${movie_release_date}'
   
    where movie_id=${movie_id}
    `
    const connection=db.openConnection()

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResult(error,result))
    })
})

router.post('/delete/:movie_id',(request,response)=>{
    const{movie_id}=request.params
  
    

    const statement=`
    delete from movie
    where movie_id=${movie_id}
    `
    const connection=db.openConnection()

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResult(error,result))
    })
})

module.exports=router;