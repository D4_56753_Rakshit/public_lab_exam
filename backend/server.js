const express = require('express')

const app = express()

const cors=require("cors")
const routerMovie=require("./Routes/movie")
app.use(cors("*"))
app.use(express.json())
app.use("/movie",routerMovie)


app.listen(4000,(request, response)=>
{
    console.log("server started on port number 4000")
})